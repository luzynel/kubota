<?php get_header(); ?>
<main class="f_site__main">
  <div class="l_wrapper">
    <div class="c_breadcrumb">
      <ul class="c_breadcrumb__li" itemscope itemtype="http://schema.org/BreadcrumbList">
          <?php if(function_exists('bcn_display')){
              bcn_display();
          }?>
      </ul>
    </div>
    <div class="c_content">
      <h2 class="c_ttl"><?php single_term_title(); ?></h2>
      <div class="c_content__desc">        
          <?php if(get_the_archive_description()) {
              echo esc_html(the_archive_description());
          } ?>

        <?php if(have_posts()) {
            while(have_posts()) { the_post();
                get_template_part('entry');
            }
        } ?>
      </div>
    </div>

  </div>  
</main>
<?php
get_footer();