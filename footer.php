    <?php if (!is_home()): ?>
    <footer class="f_site__footer">
      <div class="l_wrapper">
        <ul class="p_footer">
          <li><a href="<?php echo get_home_url(); ?>/works">works</a></li>
          <li><a href="<?php echo get_home_url(); ?>/company">company</a></li>
          <li><a href="<?php echo get_home_url(); ?>/blog">blog</a></li>
          <li><a href="<?php echo get_home_url(); ?>/contact">contact</a></li>
        </ul>
      </div>
    </footer>
    <?php endif; ?>

    <script src="<?php echo get_template_directory_uri(); ?>/js/jquery-2.1.1.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/smooth-scroll.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/plugins.js"></script>

    <script src="<?php echo get_template_directory_uri(); ?>/js/jquery.bgswitcher.js"></script>
    
    <!-- Top Slider -->
    <script type="text/javascript">
      $(document).ready(function(){
        $("body.home").bgswitcher({
          images: [
          <?php topslide(); ?>
          ],
          effect: "fade",
          easing: "swing",
          interval: 5000,
          loop: true
        });
      });
    </script>
    <style>
      div {
        background-size: cover !important;
        background-position: center !important;
      }
    </style>
    <?php wp_footer(); ?>
  </body>
</html>