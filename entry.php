<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
  <div>
    <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" rel="bookmark"><?php the_title(); ?></a>
    <p><small><?php the_time('Y.m.d'); ?></small></p>
  </div>

</article>