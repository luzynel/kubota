<?php get_header(); ?>
  <main class="f_site__main">

      <?php if(have_posts()) {
        while(have_posts()) {
        the_post(); ?>
        
        <section class="f_sec01">
          <div class="l_wrapper">
            <div class="p_company">
              <div class="c_ttl is_large"><img src="<?php echo get_template_directory_uri(); ?>/img/svg/company-logo.svg" style="max-width: 330px;" alt="<?php echo esc_html(get_bloginfo('name')); ?>"></div>
              <div class="p_company__content">
                <p>FOODIES STUDIOは、お客様のサイレントマジョリティを代弁します。<br class="pc-only">
                サイレントマジョリティとはお客様が潜在的に抱えている願望や不満のことを表します。</p>

                <p>プロのカメラマンに撮ってもらっているというだけで満足していませんか？<br class="pc-only">
                その写真のクオリティは本当に納得のいく仕上がりになっていますか？<br>
                そしてその写真が売り上げに繋がっていますか？</p>

                <p>私たちFOODIES　STUDIOはプロフェッショナルフードフォトグラファーがお客様が実現されたい商品やサービスの価値を写真<br class="pc-only">
                で具体的にご提供致します。お客様のプロフェッショナルとFOODIES STUDIOのプロフェッショナルでムーブメントを起こしま<br class="pc-only">
                しょう。どのような価値をどのような思いで世の中に送り出したいか是非お聞かせください。</p>

                <p>コンセプトは、「お客様に心からご満足頂くこと」です。</p>
              </div>

              <div class="p_company__content">
                <p>FOODIES STUDIO represents your silent majority.<br>
                Silent majority refers to the potential desires and dissatisfactions of our customers.</p>

                <p>Are you satisfied just because you have a professional photographer take the picture?<br>
                Is the quality of the photo really convincing?<br>
                And does the photo lead to sales?</p>

                <p>At FOODIES STUDIO, professional food photographers photograph the value of products and services that customers want<br class="pc-only">
                to realize.We will provide it specifically. Create a movement with your professionals and FOODIES STUDIO professionals<br class="pc-only">
                Let's do it. Please tell us what kind of value you want to send out to the world.</p>

                <p>The concept is simply "to satisfy our customers".</p>
              </div>
            </div>
          </div>
        </section>

        <section class="f_sec02">
          <div class="c_container">
            <h2 class="c_ttl">WORKS</h2>
            <div class="p_company__works">
              <div>
                <img src="<?php echo get_template_directory_uri(); ?>/img/company_thumb_01.jpg" alt="プロダクトイメージ撮影">
                <div class="c_ttl-b">プロダクトイメージ撮影</div>
                <div class="p_company__works-txt">
                  <p>最新の撮影機材を取り揃えて商品の価値や魅力を最大限に引き出し、お客様のあらゆるニーズにお応えします。</p>
                  <p>We have the latest photography equipment to maximize the value and attractiveness of our products and meet all of our customers' needs.</p>
                </div>
              </div>
              <div>
                <img src="<?php echo get_template_directory_uri(); ?>/img/company_thumb_02.jpg" alt="料理写真撮影">
                <div class="c_ttl-b">料理写真撮影</div>
                <div class="p_company__works-txt">
                  <p>料理専門のノウハウと実績をベースにお客さまの思い描くイメージを具現化します。</p>
                  <p>We will embody the image that our customers envision based on our cooking know-how and achievements.</p>
                </div>
              </div>
              <div>
                <img src="<?php echo get_template_directory_uri(); ?>/img/company_thumb_03.jpg" alt="建築写真撮影">
                <div class="c_ttl-b">建築写真撮影</div>
                <div class="p_company__works-txt">
                  <p>クオリティの高い表現力で店舗空間の魅力を最大限に引き出します。</p>
                  <p>Maximize the appeal of the store space with high-quality expressiveness.</p>
                </div>
              </div>
            </div>
          </div>
        </section>

        <section class="f_sec03">
          <div class="c_container">
            <h2 class="c_ttl">PROFILE</h2>
            <div class="p_company__profile">
              <img class="pc-only" src="<?php echo get_template_directory_uri(); ?>/img/profile_img_01.jpg" alt="久保田  翔 也  Kubota Shoya">
              <div class="l_wrapper">
                <img class="sp-only img_r" src="<?php echo get_template_directory_uri(); ?>/img/profile_img_01.jpg" style="width: 50%; max-width: 280px;" alt="久保田  翔 也  Kubota Shoya">
                <div class="c_ttl is_secondary">久保田  翔 也      <br class="sp-only">Kubota  Shoya</div>

                <p>広告写真家・フードフォトグラファー<br>
                公益社団法人 APA 日本広告写真家協会 正会員</p>

                <p>フード専門スチール撮影・動画撮影<br>
                プロフェッショナルフードフォトグラファーの第一人者としてフード広告撮影全般を手がけ、<br>
                大手食品メーカーや大手企業との取引実績も豊富。</p>

                <p>ミシュランフォトグラファー<br>
                岩手県出身 東京渋谷を拠点に活動<br>
                プロフェッショナルフード、ミシュランレストランの撮影、建築撮影、商品、広告などを主に撮影。</p>


                <p>2008年 写真家 中村淳氏に師事<br>
                2016年 SHOYA KUBOTA Photography & films設立<br>
                2017年 広告写真家 小林宗政氏に師事<br>
                2017年 岩手広告賞奨励賞受賞<br>
                2018年 PULL EXTE STYLE CONTEST 写真 特別賞<br>
                スタジオ勤務10年を得て独立。</p>

                <p>これまでの実績<br>
                2017年 岩手広告賞奨励賞<br>
                2017年 看護師協会ポスター撮影<br>
                2018年 PULL EXTE STYLE CONTEST 写真 特別賞<br>
                2018年 FUN＋WALK  (官民連携プロジェクト) CM撮影<br>
                2018年 Taipei Art Photo exhibition参加（台湾）<br>
                2018年 Dress Collection 2019 - 春夏 - 衣裳大展示会ポスター、CM撮影<br>
                2019年 LANDSCAPES EAST MEETS WEST INTERNATIONAL PHOTO EXHIBITION in ITALY 出展(イタリア）<br>
                2019年 公益財団法人 日本美術院主催 春の院展 図録撮影<br>
                2019年 Novosibirsk International festival of contemporary photography 出展(ロシア)<br>
                2019年 Los Angeles, SHATOO GALLERY出展（アメリカ）<br>
                2019年 公益財団法人 日本美術院主催 第104回院展 図録撮影<br>
                2020年 公益財団法人 日本美術院主催 春の院展 図録撮影<br>
                2020年 公益財団法人 日本美術院主催 院展 図録撮影<br>
                2021年 公益財団法人 日本美術院主催 春の院展 図録撮影</p>


                <p>ミシュランレストラン撮影<br>
                公益社団法人 日本広告写真家協会 正会員　</p>

              </div>
            </div>
          </div>
        </section>

        <section class="f_sec04">
          <div class="c_container">
            <h2 class="c_ttl">COMPANY</h2>
            <div class="p_company__info">
              <img class="pc-only" src="<?php echo get_template_directory_uri(); ?>/img/company_img_01.jpg" alt="Company Info">
              <img class="sp-only" src="<?php echo get_template_directory_uri(); ?>/img/company_img_01_sp.jpg" alt="Company Info">
              <div class="l_wrapper">
                <p><strong>KUBOTA  OFFICE</strong></p>
                <p>広告用のイメージ写真、フード写真、販売用の食材の調理写真、テレビCM、WEB動画などあらゆるフード撮影<br>
                クリエイティブディレクター、レッタッチャー、カラーコーディネーター在籍。<br>
                麻布十番直結の便利な立地にキッチンスタジオ有り。リモート撮影も対応可能。</p>

                <p>撮影機材<br>
                世界最高水準の中判デジタルカメラPhaseone XF, HASSELBLAD X1D2, LEICA Q2 etc</p>


                <p><strong>HEAD OFFICE    FOODIES STUDIO</strong><br>
                〒106-0045　東京都港区麻布十番2-21-6　アクシア麻布706<br>
                Axia-AZABU706, 2-21-6 Azabujuban, Minato-ku, Tokyo</p>

                <p>TEL  <a href="tel:09042870026" target="_blank">090-4287-0026</a></p>

                <p>&nbsp;</p>
                <p>主な取引先<br>
                株式会社モスフードサービス、ソフトバンクグループ、ゼクシィ、東レ株式会社、みずほ銀行、日本美術院、<br>
                HOTEL CHOCOLAT、Chocolate Akademy バリーカレボー社、DAVID MYERS、The Momentum by Porsche(ポルシェカフェ)</p>

              </div>
            </div>
          </div>
          <div class="p_company__map">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3241.9268766591813!2d139.73433341557003!3d35.6541731890623!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xaacb719eafb4955f!2z44Ki44Kv44K344Ki6bq75biD!5e0!3m2!1sen!2sph!4v1632302526656!5m2!1sen!2sph" width="100%" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
              <div class="p_company__btn"><a class="c_btn" href="<?php echo get_home_url(); ?>/contact">CONTACT</a></div>
          </div>
        </section>

      <?php   
        }
      } ?>
          
  </main>
<?php
get_footer();