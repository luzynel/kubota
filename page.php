<?php get_header(); ?>
  <main class="f_site__main">
    <div class="l_wrapper">

      <?php if(have_posts()) {
        while(have_posts()) {
        the_post(); 
        $pimg = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' ); ?>
        <div class="c_breadcrumb">
          <ul class="c_breadcrumb__li" itemscope itemtype="http://schema.org/BreadcrumbList">
              <?php if(function_exists('bcn_display')){
                  bcn_display();
              }?>
          </ul>
        </div>
        <div class="c_content">
          <h2 class="c_ttl"><?php the_title(); ?></h2>
          <div class="c_content__desc">
            <?php if ( has_post_thumbnail() ){ echo '<img class="c_content__img is_center" src="'.$pimg[0].'" alt="'.get_the_title().'">'; } ?>
            <?php the_content(); ?>
          </div>
        </div>
      <?php   
        }
      } ?>
      
    </div>      
  </main>
<?php
get_footer();