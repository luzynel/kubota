<!DOCTYPE html>
<html <?php language_attributes(); ?>>
  <head>
    <meta charset="<?php bloginfo('charset'); ?>" />
    <meta name="viewport" content="width=device-width" />

    <?php if (is_home()):?>
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/JiSlider.css" type="text/css" media="all">
    <?php endif; ?>

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/style.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/common_header.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/common_footer.css">
    <?php wp_head(); ?>
  </head>
  <body <?php body_class(); ?>>

      <!-- Header -->
      <div class="f_site__header clearfix">
        <header class="p_header<?php if (!is_home()){ echo ' p_header-inner'; } ?>">
          <div class="p_header__ham">
            <div class="is_toggle is_navOp">
              <div><span></span><span></span><span></span></div>
            </div>
          </div>
          <div class="p_header__logo">      
            <?php if(is_home()): ?>
              <h1><a href="<?php echo esc_url(home_url('/')); ?>">
                <img class="is_inactive" src="<?php echo get_template_directory_uri(); ?>/img/svg/logo.svg" width="350px" alt="<?php echo esc_html(get_bloginfo('name')); ?>">
                <img class="is_active" src="<?php echo get_template_directory_uri(); ?>/img/svg/logo-inner.svg" style="max-width:100%; margin:0 auto;" width="330px" alt="<?php echo esc_html(get_bloginfo('name')); ?>">
              </a></h1>
            <?php else: ?>
            <a href="<?php echo esc_url(home_url('/')); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/svg/logo-inner.svg" width="330px" alt="<?php echo esc_html(get_bloginfo('name')); ?>"></a>
            <?php endif; ?>  
          


            
          </div>
          <div class="p_header__icons">
            <ul>
              <li><a href="http://www.instagram.com" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/svg/icon_ig.svg" width="60px" alt="Instagram"></a></li>
              <li><a href="<?php echo get_home_url(); ?>/contact"><img src="<?php echo get_template_directory_uri(); ?>/img/svg/icon_enve.svg" width="60px" alt="Contact Us"></a></li>
            </ul>
          </div>
        </header>
        <nav class="p_header__nav">
          <div class="is_navflex">
            <?php if ( !is_home() && !is_single() && !is_404() ): ?>
            <ul class="is_active">
              <li>
              <?php 
                if(is_category()):
                  single_term_title();
                else: 
                  the_title(); 
                endif;
              ?>
              </li>
            </ul>
            <?php endif; ?>
            <?php
              wp_nav_menu( array(
                'container' => false,
                'menu_class' => 'p_header__nav-list',
                'menu_id' => ''
              ) );
            ?>
            <ul class='p_header__nav-icons'>
              <li><a href="http://www.instagram.com" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/svg/icon_ig.svg" width="60px" alt="Instagram"></a></li>
              <li><a href="<?php echo get_home_url(); ?>/contact"><img src="<?php echo get_template_directory_uri(); ?>/img/svg/icon_enve.svg" width="60px" alt="Contact Us"></a></li>
            </ul>
          </div>
        </nav>
      </div>
      <!-- .f_site__header -->