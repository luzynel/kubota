$(document).ready(function(){
	$('.p_work .vp-search input').after('<span class="vp-search-btn">&nbsp;</span>');
	$('.p_work .vp-search input').removeClass('open');
});


$(window).on('resize',function(){
	// ***************************************Check Window Width

	var nav = $(".p_header__nav.open .is_navflex").height() + 80; //console.log(nav);
	var navhead = $(".p_header__nav.open").height(); //console.log(navhead);

	if (navhead < nav) {
		$(".p_header__nav").removeClass('is_scroll');
		$(".p_header__nav.open").addClass('is_scroll');
	}else {
		$(".p_header__nav").removeClass('is_scroll');
	}

});

// SP Menu Toggle
$('.is_toggle').click(function(){
	$(this).toggleClass('active');
	$(this).parents('.f_site__header').toggleClass('open');
	$('.p_header__nav').toggleClass('open');
	$('.p_header__logo img.is_active').toggleClass('show');
	$('.p_header__logo img.is_inactive').toggleClass('hide');

	var nav = $(".p_header__nav.open .is_navflex").height() + 80; //console.log(nav);
	var navhead = $(".p_header__nav.open").height(); //console.log(navhead);

	if (navhead < nav) {
		$(".p_header__nav").removeClass('is_scroll');
		$(".p_header__nav.open").addClass('is_scroll');
	}else {
		$(".p_header__nav").removeClass('is_scroll');
	}
});
$('.vp-search').on('click', '.vp-search-btn', function(){
	$(this).siblings('input').toggleClass('open');
});


// Main Navigation Current Setting
$(function() {
	$('.menu li a').each(function(){
			var $href = $(this).attr('href');
			if(location.href.match($href)) {
					$(this).parent().addClass('current');
			} else {
					$(this).parent().removeClass('current');
			}
	});
});

// Smooth Scroll
$(function() {
	var scroll = new SmoothScroll('.js_scroll', {
		speed: 500,//スクロールする速さ
		header: '.p_header'//固定ヘッダーがある場合
	});
});

var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36251023-1']);
  _gaq.push(['_setDomainName', 'jqueryscript.net']);
  _gaq.push(['_trackPageview']);

(function() {
	var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();