<?php get_header(); ?>
<main class="f_site__main">
    <div class="l_wrapper">
        <div class="c_breadcrumb">
            <ul class="c_breadcrumb__li" itemscope itemtype="http://schema.org/BreadcrumbList">
                <?php if(function_exists('bcn_display')){
                    bcn_display();
                }?>
            </ul>
        </div>
        <div class="c_content">
          <h2 class="c_ttl"><?php esc_html_e('Not Found','simplyblankslate'); ?></h2>
          <div class="c_content__desc">
                <p>お探しのページはありませんでした</p>
          </div>
        </div>
    </div>
</main>
<?php 
get_footer();