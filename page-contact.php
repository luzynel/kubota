<?php get_header(); ?>
  <main class="f_site__main">
    <div class="l_wrapper">
        <div class="c_breadcrumb">
          <ul class="c_breadcrumb__li" itemscope itemtype="http://schema.org/BreadcrumbList">
              <?php if(function_exists('bcn_display')){
                  bcn_display();
              }?>
          </ul>
        </div>

      <?php if(have_posts()) {
        while(have_posts()) { the_post(); ?>
        
        <div class="c_content">
          <h2 class="c_ttl"><?php the_title(); ?></h2>
          <div class="c_content__desc">
            <?php the_content(); ?>            
          </div>
        </div>
      <?php   
        }
      } ?>
      
    </div>      
  </main>
<?php
get_footer();