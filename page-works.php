<?php get_header(); ?>
  <main class="f_site__main">
  <?php 
    if(have_posts()) {
      while(have_posts()) {
        the_post(); 

        the_content();
  ?>

  <?php 
      }
    }
  ?>
  </main>

<?php
get_footer();